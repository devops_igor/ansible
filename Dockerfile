FROM ubuntu:focal
RUN apt update
RUN apt install python3-pip apache2-utils -y
RUN pip install ansible ansible-lint yamllint