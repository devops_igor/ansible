# Pulls an image of RabbitMQ for {{ platform }} and deploys it as docker container
# with {{ container.name }} and {{ container.hostname }}
# Usage: ansible-playbook example.yml -e platform=amd64 -e container.name=some-rabbit -e container.hostname=my-rabbit
