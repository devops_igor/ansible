# Creates directory in {{ base_root.dir }} path and {{ base_root.mode }} permissions
# Usage: ansible-playbook example.yml -e base_root.dir=/tmp/mydir -e base_root.mode=0755
